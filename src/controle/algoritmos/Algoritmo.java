package controle.algoritmos;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import modelo.Particula;
import modelo.Posicao;
import visao.Janela;
import controle.funcoes.Funcao;
import controle.persistencia.IGuarda;

public abstract class Algoritmo implements Runnable {

	protected double w, meritoGlobal, velMaxima;
	protected int iteracao, maxIteracoes, sleep, np;
	protected List<Particula> enxame;
	protected Posicao minimos, maximos, melhorGlobal;
	protected Funcao funcao;
	protected Janela janela;
	protected Random random;
	protected boolean continuar;
//	protected IGuarda guarda;
	protected Posicao centroMassa;

	public Algoritmo(Funcao funcao, Janela janela, IGuarda guarda) {
		w = 0.9F;
		sleep = 64;
		this.janela = janela;
		melhorGlobal = new Posicao();
		minimos = new Posicao(100, 100);
		maximos = new Posicao(1000, 600);
		this.funcao = funcao;
		enxame = new ArrayList<Particula>();
		random = new Random();
		continuar = true;
		this.meritoGlobal = Double.MAX_VALUE;
		this.maxIteracoes = 300;
		this.np = 20;
		velMaxima = Math.min(funcao.getMaximo().getX()
				- funcao.getMinimo().getX(), funcao.getMaximo().getY()
				- funcao.getMinimo().getY())
				/ np;

//		this.guarda = guarda;
	}

	public void execute() {
		inicializa();
		new Thread(this).start();
	}

	@Override
	public void run() {
		janela.desenha();

		// @VIDEO
		// try {
		// Process proc;
		// try {
		// Thread.sleep(500); //Tempo que a janela leva para abrir antes de
		// começar a gravar
		// proc = Runtime
		// .getRuntime()
		// .exec("recordmydesktop -x 320 -y 160 --width 900 --height 500 -o /home/gian/video.ogv --no-sound");
		// } catch (IOException | InterruptedException e) {
		// e.printStackTrace();
		// }
		// Thread.sleep(sleep*10);
		// Thread.sleep(sleep);
		// } catch (InterruptedException e) {
		// e.printStackTrace();
		// }

		for (iteracao = 0; iteracao < maxIteracoes; iteracao++) {
			janela.desenha();
			try {
				Thread.sleep(sleep);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
//			guarda.guarda(this.getEnxame());
			calcula();
			movimenta();
		}
//		guarda.encerra();
	}

	// Metodos abstratos
	public abstract void movimenta();

	public abstract void calcula();

	// Metodos de acesso

	public int getIteracao() {
		return iteracao;
	}

	public void setIteracao(int iteracao) {
		this.iteracao = iteracao;
	}

	public double getMeritoGlobal() {
		return meritoGlobal;
	}

	public void setMeritoGlobal(double meritoGlobal) {
		this.meritoGlobal = meritoGlobal;
	}

	public double getVelMaxima() {
		return velMaxima;
	}

	public void setVelMaxima(double velMaxima) {
		this.velMaxima = velMaxima;
	}

	public Posicao getMelhorGlobal() {
		return melhorGlobal;
	}

	public void setMelhorGlobal(Posicao melhorGlobal) {
		this.melhorGlobal = melhorGlobal;
	}

	public Posicao getMinimos() {
		return minimos;
	}

	public void setMinimos(Posicao minimos) {
		this.minimos = minimos;
	}

	public Posicao getMaximos() {
		return maximos;
	}

	public void setMaximos(Posicao maximos) {
		this.maximos = maximos;
	}

	public List<Particula> getEnxame() {
		return enxame;
	}

	public void setEnxame(List<Particula> enxame) {
		// this.enxame = enxame;
		for (Particula particula : enxame) {
			this.enxame.add(particula);
		}
	}

	public Funcao getFuncao() {
		return funcao;
	}

	public void setFuncao(Funcao funcao) {
		this.funcao = funcao;
	}

	public void mantemNaTela(Particula particula) {
		if (particula.getPosicao().getX() < funcao.getMinimo().getX()) {
			particula.getPosicao().setX(funcao.getMinimo().getX());
			particula.getVelocidade().setX(-particula.getVelocidade().getX());
		} else if (particula.getPosicao().getX() > funcao.getMaximo().getX()) {
			particula.getPosicao().setX(funcao.getMaximo().getX());
			particula.getVelocidade().setX(-particula.getVelocidade().getX());
		}
		if (particula.getPosicao().getY() < funcao.getMinimo().getY()) {
			particula.getPosicao().setY(funcao.getMinimo().getY());
			particula.getVelocidade().setY(-particula.getVelocidade().getY());
		} else if (particula.getPosicao().getY() > funcao.getMaximo().getY()) {
			particula.getPosicao().setY(funcao.getMaximo().getY());
			particula.getVelocidade().setY(-particula.getVelocidade().getY());
		}
	}

	public void velocidadeLimite(Particula particula) {
		if (Math.abs(particula.getVelocidade().getX()) > velMaxima) {
			particula.getVelocidade().setX(
					Math.abs(particula.getVelocidade().getX())
							/ particula.getVelocidade().getX() * velMaxima);
		}
		if (Math.abs(particula.getVelocidade().getY()) > velMaxima) {
			particula.getVelocidade().setY(
					Math.abs(particula.getVelocidade().getY())
							/ particula.getVelocidade().getY() * velMaxima);
		}

	}

	public abstract void inicializa();

	public void stop() {
		continuar = false;
	}

	public Posicao getCentroMassa() {
		Posicao centroMassa = new Posicao(0, 0);
		for (Particula p : enxame) {
			centroMassa.setX(centroMassa.getX() + p.getPosicao().getX());
			centroMassa.setY(centroMassa.getY() + p.getPosicao().getY());
		}
		centroMassa.setX(centroMassa.getX() / (enxame.size()));
		centroMassa.setY(centroMassa.getY() / (enxame.size()));
		this.setCentroMassa(centroMassa);
		return this.centroMassa;
	}

	public void setCentroMassa(Posicao centroMassa) {
		this.centroMassa = centroMassa;
	}

	public int getMaxIteracoes() {
		return maxIteracoes;
	}

}

package controle.algoritmos;

import java.util.ArrayList;
import java.util.List;

import modelo.Passaro;
import modelo.Pirilampo;
import modelo.Posicao;
import visao.Janela;
import controle.funcoes.Funcao;
import controle.persistencia.IGuarda;

public class Boids extends Algoritmo {

	private List<Passaro> passaros;
	private Pirilampo objetivo;

	public Boids(Funcao funcao, Janela janela, IGuarda guarda) {
		super(funcao, janela, guarda);
		super.np = 20;
	}

	@Override
	public void movimenta() {
		for (Passaro passaro : passaros) {
			passaro.movimenta();
		}
	}

	@Override
	public void calcula() {
		for (Passaro passaro : passaros) {
			passaro.calcula(passaros);
		}
	}

	@Override
	public void inicializa() {
		iteracao = 0;
		passaros = new ArrayList<Passaro>();
		int j = (int) (random.nextDouble() * funcao.getOtimos().size());
		if (funcao.getOtimos().size() == 0)
			objetivo = new Pirilampo(new Posicao());
		else
			objetivo = new Pirilampo(funcao.getOtimos().get(j));
		for (int i = 0; i < np; i++) {
			passaros.add(new Passaro(new Posicao(getFuncao().getMinimo().getX()
					+ Math.abs(random.nextDouble())
					* (getFuncao().getMaximo().getX() - getFuncao().getMinimo()
							.getX()), getFuncao().getMinimo().getY()
					+ Math.abs(random.nextDouble())
					* (getFuncao().getMaximo().getY() - getFuncao().getMinimo()
							.getY())), objetivo));
		}

		enxame.addAll(passaros);
	}
}

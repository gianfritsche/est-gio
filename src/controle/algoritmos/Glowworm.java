package controle.algoritmos;

import modelo.Particula;
import modelo.Pirilampo;
import modelo.Posicao;
import visao.Janela;
import controle.funcoes.Funcao;
import controle.persistencia.IGuarda;

public class Glowworm extends Algoritmo {

	public Glowworm(Funcao funcao, Janela janela, IGuarda guarda) {
		super(funcao, janela, guarda);
		maxIteracoes = 300;
		np=40;
		velMaxima = Math.min(funcao.getMaximo().getX()
				- funcao.getMinimo().getX(), funcao.getMaximo().getY()
				- funcao.getMinimo().getY())/np;
	}

	@Override
	public void movimenta() {
		for (Particula particula : enxame) {
			Pirilampo pirilampo = (Pirilampo) particula;
			pirilampo.determinaVizinhos(enxame);
			pirilampo.atualizaPosicao();
			pirilampo.atualizaRaio();
		}
	}

	@Override
	public void calcula() {
		avaliacao();
		for (Particula particula : enxame) {
			Pirilampo pirilampo = (Pirilampo) particula;
			pirilampo.atualizaLuciferin();
		}

	}

	@Override
	public void inicializa() {
		iteracao = 0;
		for (int i = 0; i < np; i++) {
			enxame.add(new Pirilampo(new Posicao(getFuncao().getMinimo().getX()
					+ Math.abs(random.nextDouble())
					* (getFuncao().getMaximo().getX() - getFuncao().getMinimo()
							.getX()), getFuncao().getMinimo().getY()
					+ Math.abs(random.nextDouble())
					* (getFuncao().getMaximo().getY() - getFuncao().getMinimo()
							.getY()))));
		}
	}

	public void avaliacao() {
		for (Particula particula : enxame) {
			particula.setMerito(funcao.execute(particula.getPosicao().getX(),
					particula.getPosicao().getY()));
		}
	}

}

package controle.algoritmos;

import java.util.ArrayList;
import java.util.List;

import modelo.Particula;
import modelo.Passaro;
import modelo.Pirilampo;
import modelo.Posicao;
import visao.Janela;
import controle.funcoes.Funcao;
import controle.persistencia.IGuarda;

public class GlowwormBoids extends Algoritmo {

	private List<List<Passaro>> subEnxames;
	private List<Particula> enxamePrincipal;

	private int nsp; // número de particulas por subEnxame

	public GlowwormBoids(Funcao funcao, Janela janela, IGuarda guarda) {
		super(funcao, janela, guarda);
		subEnxames = new ArrayList<List<Passaro>>();
		maxIteracoes = 300;
		np = 10;
		nsp = 3;
	}

	@Override
	public void calcula() { // BOIDS
		for (int i = 0; i < subEnxames.size(); i++) {
			calculaBoids(subEnxames.get(i));
			movimentaBoids(subEnxames.get(i));
		}
	}

	private void movimentaBoids(List<Passaro> list) {
		for (Passaro passaro : list) {
			passaro.movimenta();
		}
	}

	private void calculaBoids(List<Passaro> list) {
		for (Passaro passaro : list) {
			passaro.calcula(list);
		}
	}

	@Override
	public void movimenta() { // GLOWWORM
		defineEnxame();
		calculaGlowworm(enxamePrincipal);
		movimentaGlowworm(enxamePrincipal);
	}

	@Override
	public void inicializa() {
		iteracao = 0;
		enxamePrincipal = new ArrayList<Particula>();
		for (int i = 0; i < np; i++) {
			subEnxames.add(new ArrayList<Passaro>());
			Pirilampo p = new Pirilampo(new Posicao(getFuncao().getMinimo()
					.getX()
					+ Math.abs(random.nextDouble())
					* (getFuncao().getMaximo().getX() - getFuncao().getMinimo()
							.getX()), getFuncao().getMinimo().getY()
					+ Math.abs(random.nextDouble())
					* (getFuncao().getMaximo().getY() - getFuncao().getMinimo()
							.getY())));
			enxamePrincipal.add(p);
			p.setMerito(Double.MAX_VALUE);
			for (int j = 0; j < nsp; j++) {

				double t = 2.0 * Math.PI * random.nextDouble();
				double u = random.nextDouble() + random.nextDouble();
				double r = u > 1 ? 2 - u : u;
				subEnxames.get(i).add(
						new Passaro(new Posicao(r * Math.cos(t) * 3.0
								+ p.getPosicao().getX(), r * Math.sin(t) * 3.0
								+ p.getPosicao().getY()),p));
			}
		}

		// apenas para fins de desenho
		enxame = new ArrayList<Particula>();
		enxame.addAll(enxamePrincipal);
		for (List<Passaro> lista : subEnxames) {
			enxame.addAll(lista);
		}
	}

	public void movimentaGlowworm(List<Particula> enxame) {
		for (Particula particula : enxame) {
			Pirilampo pirilampo = (Pirilampo) particula;
			pirilampo.determinaVizinhos(enxame);
			pirilampo.atualizaPosicao();
			pirilampo.atualizaRaio();
		}
	}

	public void calculaGlowworm(List<Particula> enxame) {
		avaliacao();
		for (Particula particula : enxame) {
			((Pirilampo) particula).atualizaLuciferin();
		}
	}

	public void avaliacao() {
		for (Particula particula : enxamePrincipal) {
			particula.setMerito(funcao.execute(particula.getPosicao().getX(),
					particula.getPosicao().getY()));
		}
		for (List<Passaro> sub : subEnxames) {
			for (Particula particula : sub) {
				particula.setMerito(funcao.execute(particula.getPosicao().getX(),
						particula.getPosicao().getY()));
				
			}
		}
	}

	public void defineEnxame() {
		int i = 0;
		for (List<Passaro> lista : subEnxames) {
			Passaro melhor = lista.get(0);
			for (Passaro passaro : lista) {
				if (passaro.getMerito() < melhor.getMerito()) {
					melhor = passaro;
				}
			}

			if (melhor.getMerito() < enxamePrincipal.get(i).getMerito()) {
				Posicao p = new Posicao(melhor.getPosicao().getX(), melhor.getPosicao().getY());
				melhor.setPosicao(enxamePrincipal.get(i).getPosicao());
				enxamePrincipal.get(i).setPosicao(p);
			}
			i++;
		}
	}
}

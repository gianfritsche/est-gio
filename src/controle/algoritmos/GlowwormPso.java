package controle.algoritmos;

import java.util.ArrayList;
import java.util.List;

import modelo.Particula;
import modelo.Pirilampo;
import modelo.Posicao;
import visao.Janela;
import controle.funcoes.Funcao;
import controle.persistencia.IGuarda;

public class GlowwormPso extends Algoritmo {

	private List<List<Pirilampo>> subEnxames;

	private int nsp; // número de particulas por subEnxame

	public GlowwormPso(Funcao funcao, Janela janela, IGuarda guarda) {
		super(funcao, janela, guarda);
		subEnxames = new ArrayList<List<Pirilampo>>();
		maxIteracoes = 600;
		np = 10;
		nsp = 6;
	}

	@Override
	public void calcula() { //PSO
		for(int i=0;i<subEnxames.size();i++){
			calculaPso(subEnxames.get(i), enxame.get(i).getMerito(),enxame.get(i).getPosicao());
			movimentaPso(subEnxames.get(i), enxame.get(i).getMerito(),enxame.get(i).getPosicao());
		}
	}
	
	@Override
	public void movimenta() { //GLOWWORM
		calculaGlowworm(enxame);
		movimentaGlowworm(enxame);
	}

	@Override
	public void inicializa() {
		iteracao = 0;
		enxame = new ArrayList<Particula>();
		for (int i = 0; i < np; i++) {
			subEnxames.add(new ArrayList<Pirilampo>());
			enxame.add(new Pirilampo(new Posicao()));
			enxame.get(i).setMerito(Double.MAX_VALUE);
			for (int j = 0; j < nsp; j++) {
				subEnxames.get(i).add(
						new Pirilampo(new Posicao(getFuncao().getMinimo()
								.getX()
								+ Math.abs(random.nextDouble())
								* (getFuncao().getMaximo().getX() - getFuncao()
										.getMinimo().getX()), getFuncao()
								.getMinimo().getY()
								+ Math.abs(random.nextDouble())
								* (getFuncao().getMaximo().getY() - getFuncao()
										.getMinimo().getY()))));
			}
		}
	}

	public void calculaPso(List<Pirilampo> enxame, Double meritoGlobal,
			Posicao melhorGlobal) {
		Posicao aux = new Posicao();
		double resultado = 0;
		for (Particula particula : enxame) {
			aux = particula.getPosicao();
			resultado = funcao.execute(aux.getX(), aux.getY());

			if (resultado < particula.getMerito()) {
				particula.getMelhor().setX(particula.getPosicao().getX());
				particula.getMelhor().setY(particula.getPosicao().getY());
				particula.setMerito(resultado);
				if (resultado < meritoGlobal) {
					melhorGlobal.setX(particula.getPosicao().getX());
					melhorGlobal.setY(particula.getPosicao().getY());
					meritoGlobal = resultado;
				}
			}
		}
	}

	public void movimentaPso(List<Pirilampo> enxame, double meritoGlobal,
			Posicao melhorGlobal) {
		w = (0.9 - (iteracao / maxIteracoes) * 0.5);
		for (Particula particula : enxame) {
			particula
					.getVelocidade()
					.setX(w
							* particula.getVelocidade().getX()
							+ (2.0 * random.nextDouble() * (melhorGlobal.getX() - particula
									.getPosicao().getX()))
							+ (2.0 * random.nextDouble() * (particula.getMelhor()
									.getX() - particula.getPosicao().getX())));
			particula
					.getVelocidade()
					.setY(w
							* particula.getVelocidade().getY()
							+ (2.0 * random.nextDouble() * (melhorGlobal.getY() - particula
									.getPosicao().getY()))
							+ (2.0 * random.nextDouble() * (particula.getMelhor()
									.getY() - particula.getPosicao().getY())));

//			velocidadeLimite(particula);
			particula.getPosicao().setX(
					particula.getPosicao().getX()
							+ particula.getVelocidade().getX());
			particula.getPosicao().setY(
					particula.getPosicao().getY()
							+ particula.getVelocidade().getY());
//			mantemNaTela(particula);
		}
	}

	public void movimentaGlowworm(List<Particula> enxame) {
		for (Particula particula : enxame) {
			Pirilampo pirilampo = (Pirilampo) particula;
			pirilampo.determinaVizinhos(enxame);
			System.out.println(pirilampo.getR());
			pirilampo.atualizaPosicao();
			pirilampo.atualizaRaio();
		}
	}

	public void calculaGlowworm(List<Particula> enxame) {
		for (Particula particula : enxame) {
			((Pirilampo)particula).atualizaLuciferin();
		}
	}
}

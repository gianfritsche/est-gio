package controle.algoritmos;

import modelo.Particula;
import modelo.Posicao;
import visao.Janela;
import controle.funcoes.Funcao;
import controle.persistencia.IGuarda;

public class PSO extends Algoritmo {

	// Construtor
	public PSO(Funcao funcao, Janela janela, IGuarda guarda) {
		super(funcao, janela, guarda);
		iteracao = 0;
		np = 50;
		velMaxima = Math.min(funcao.getMaximo().getX()
				- funcao.getMinimo().getX(), funcao.getMaximo().getY()
				- funcao.getMinimo().getY())
				/ np;
	}

	// Metodos da interface Algoritmo
	@Override
	public void movimenta() {
		w = (float) (0.9 - (iteracao / 200.0) * 0.5);

		for (Particula particula : enxame) {

			particula
					.getVelocidade()
					.setX(w
							* particula.getVelocidade().getX()
							+ (2 * random.nextFloat() * (melhorGlobal.getX() - particula
									.getPosicao().getX()))
							+ (2 * random.nextFloat() * (particula.getMelhor()
									.getX() - particula.getPosicao().getX())));
			particula
					.getVelocidade()
					.setY(w
							* particula.getVelocidade().getY()
							+ (2 * random.nextFloat() * (melhorGlobal.getY() - particula
									.getPosicao().getY()))
							+ (2 * random.nextFloat() * (particula.getMelhor()
									.getY() - particula.getPosicao().getY())));

			velocidadeLimite(particula);

			particula.getPosicao().setX(
					particula.getPosicao().getX()
							+ particula.getVelocidade().getX());
			particula.getPosicao().setY(
					particula.getPosicao().getY()
							+ particula.getVelocidade().getY());

			mantemNaTela(particula);
		}
	}

	@Override
	public void calcula() {
		Posicao aux = new Posicao();
		double resultado = 0;
		for (Particula particula : enxame) {
			aux = particula.getPosicao();
			resultado = funcao.execute(aux.getX(), aux.getY());

			if (resultado < particula.getMerito()) {
				particula.getMelhor().setX(particula.getPosicao().getX());
				particula.getMelhor().setY(particula.getPosicao().getY());
				particula.setMerito(resultado);
				if (resultado < meritoGlobal) {
					melhorGlobal.setX(particula.getPosicao().getX());
					melhorGlobal.setY(particula.getPosicao().getY());
					meritoGlobal = resultado;
				}
			}
		}
	}

	@Override
	public void inicializa() {
		meritoGlobal = Double.MAX_VALUE;
		for (int i = 0; i < np; i++) {
			enxame.add(new Particula(new Posicao(getFuncao().getMinimo().getX()
					+ Math.abs(random.nextDouble())
					* (getFuncao().getMaximo().getX() - getFuncao().getMinimo()
							.getX()), getFuncao().getMinimo().getY()
					+ Math.abs(random.nextDouble())
					* (getFuncao().getMaximo().getY() - getFuncao().getMinimo()
							.getY()))));
		}
	}

}

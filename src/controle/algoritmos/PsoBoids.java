package controle.algoritmos;

import modelo.Boid;
import modelo.Particula;
import modelo.Posicao;
import visao.Janela;
import controle.funcoes.Funcao;
import controle.persistencia.IGuarda;

public class PsoBoids extends Algoritmo {

	private double distancia, w1, w2, w3, w4;

	public PsoBoids(Funcao funcao, Janela janela, IGuarda guarda) {
		super(funcao, janela, guarda);
		w1 = 0.25;
		w2 = 0.25;
		w3 = 0.25;
		w4 = 0.25;
		np = 20;
		distancia = Math.min(funcao.getMaximo().getX()
				- funcao.getMinimo().getX(), funcao.getMaximo().getY()
				- funcao.getMinimo().getY())
				/ np;
		velMaxima = Math.min(funcao.getMaximo().getX()
				- funcao.getMinimo().getX(), funcao.getMaximo().getY()
				- funcao.getMinimo().getY())
				/ np;
	}

	@Override
	public void movimenta() {
		w = (float) (0.9 - (iteracao / 200.0) * 0.5);
		Boid boid;
		for (Particula particula : enxame) {
			boid = (Boid) particula;

			/*
			 * velocidade = w velocidade + w1 v1 + w2 v2 + w3 v3 + w4 v4
			 */

			particula.getVelocidade().setX(
					w * particula.getVelocidade().getX() + w1
							* boid.getV1().getX() + w2 * boid.getV2().getX()
							+ w3 * boid.getV3().getX() + w4
							* boid.getV4().getX());
			particula.getVelocidade().setY(
					w * particula.getVelocidade().getY() + w1
							* boid.getV1().getY() + w2 * boid.getV2().getY()
							+ w3 * boid.getV3().getY() + w4
							* boid.getV4().getY());

			velocidadeLimite(particula);

			/* posicao = posicao + velocidade */
			particula.getPosicao().setX(
					particula.getPosicao().getX()
							+ particula.getVelocidade().getX());
			particula.getPosicao().setY(
					particula.getPosicao().getY()
							+ particula.getVelocidade().getY());

			mantemNaTela(particula);
		}

	}

	@Override
	public void calcula() {
		Boid boid;
		pso();
		for (Particula particula : enxame) {
			boid = (Boid) particula;
			boid.setV1(regra1(particula));
			boid.setV2(regra2(particula));
			boid.setV3(regra3(particula));
			boid.setV4(regraPSO(particula));
		}
	}

	public void pso() {
		Posicao aux = new Posicao();
		double resultado = 0;
		for (Particula particula : enxame) {
			aux = particula.getPosicao();
			resultado = funcao.execute(aux.getX(), aux.getY());

			if (resultado < particula.getMerito()) {
				particula.getMelhor().setX(particula.getPosicao().getX());
				particula.getMelhor().setY(particula.getPosicao().getY());
				particula.setMerito(resultado);
				if (resultado < meritoGlobal) {
					melhorGlobal.setX(particula.getPosicao().getX());
					melhorGlobal.setY(particula.getPosicao().getY());
					meritoGlobal = resultado;
				}
			}
		}
	}

	private Posicao regraPSO(Particula particula) {
		Posicao posicao = new Posicao();
		posicao.setX((2 * random.nextFloat() * (melhorGlobal.getX() - particula
				.getPosicao().getX()))
				+ (2 * random.nextFloat() * (particula.getMelhor().getX() - particula
						.getPosicao().getX())));
		posicao.setY((2 * random.nextFloat() * (melhorGlobal.getY() - particula
				.getPosicao().getY()))
				+ (2 * random.nextFloat() * (particula.getMelhor().getY() - particula
						.getPosicao().getY())));
		return posicao;
	}

	private Posicao regra1(Particula particula) {
		Posicao centroMassa = new Posicao(0, 0);
		for (Particula p : enxame) {
			if (!particula.equals(p)) {
				centroMassa.setX(centroMassa.getX() + p.getPosicao().getX());
				centroMassa.setY(centroMassa.getY() + p.getPosicao().getY());
			}
		}
		centroMassa.setX(centroMassa.getX() / (enxame.size() - 1));
		centroMassa.setY(centroMassa.getY() / (enxame.size() - 1));
		centroMassa.setX(centroMassa.getX() - particula.getPosicao().getX());
		centroMassa.setY(centroMassa.getY() - particula.getPosicao().getY());
		return centroMassa;
	}

	private Posicao regra2(Particula particula) {
		Posicao posicao = new Posicao(0, 0);
		double x, y;
		for (Particula p : enxame) {
			double dist_euc = 0;
			if (!particula.equals(p)) {
				x = particula.getPosicao().getX() - p.getPosicao().getX();
				x *= x;
				y = particula.getPosicao().getY() - p.getPosicao().getY();
				y *= y;
				dist_euc = Math.sqrt(x + y);
				if (dist_euc < distancia) {
					posicao.setX(posicao.getX()
							- ((p.getPosicao().getX() - particula.getPosicao()
									.getX())));
					posicao.setY(posicao.getY()
							- ((p.getPosicao().getY() - particula.getPosicao()
									.getY())));
				}
			}
		}
		return posicao;

	}

	private Posicao regra3(Particula particula) {
		Posicao soma = new Posicao(0, 0);
		for (Particula p : enxame) {
			if (!particula.equals(p)) {
				soma.setX(soma.getX() + p.getVelocidade().getX());
				soma.setY(soma.getY() + p.getVelocidade().getY());
			}
		}
		soma.setX((soma.getX() / (enxame.size() - 1)));
		soma.setY((soma.getY() / (enxame.size() - 1)));
		return soma;
	}

	@Override
	public void inicializa() {
		meritoGlobal = Double.MAX_VALUE;
		for (int i = 0; i < np; i++) {
			enxame.add(new Boid(new Posicao(getFuncao().getMinimo().getX()
					+ Math.abs(random.nextDouble())
					* (getFuncao().getMaximo().getX() - getFuncao().getMinimo()
							.getX()), getFuncao().getMinimo().getY()
					+ Math.abs(random.nextDouble())
					* (getFuncao().getMaximo().getY() - getFuncao().getMinimo()
							.getY()))));
		}
	}

}

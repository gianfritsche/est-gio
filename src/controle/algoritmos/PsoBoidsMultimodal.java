package controle.algoritmos;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import modelo.Particula;
import modelo.Posicao;
import modelo.Presa;
import visao.Janela;
import controle.funcoes.Funcao;
import controle.persistencia.IGuarda;

public class PsoBoidsMultimodal extends Algoritmo {

	private List<Posicao> predadores;
	private int  iteracoesSemMelhoria;
	private double distancia, w1, w2, w3, w4, w5, repulsao, c1, c2;
	private boolean generalista;
	private Color cor;

	public PsoBoidsMultimodal(Funcao funcao, Janela janela, IGuarda guarda, Color cor) {
		super(funcao, janela, guarda);
		distancia = Math.min(funcao.getMaximo().getX()
				- funcao.getMinimo().getX(), funcao.getMaximo().getY()
				- funcao.getMinimo().getY())/np;
		repulsao = Math.min(funcao.getMaximo().getX()
				- funcao.getMinimo().getX(), funcao.getMaximo().getY()
				- funcao.getMinimo().getY())/np;
		setW1(0.9);// aproximação
		setW2(0.9);// repulsão
		setW3(0.9);// coesão
		setW4(0.0);// pso
		setW5(0.9);// evitar predadores
		setC1(2.0); // componente individual
		setC2(1.4); // componente global
		np = 20;
		this.predadores = new ArrayList<Posicao>();
		this.cor = cor;
		this.iteracoesSemMelhoria = 0;
		generalista = true;
		velMaxima = Math.min(funcao.getMaximo().getX()
				- funcao.getMinimo().getX(), funcao.getMaximo().getY()
				- funcao.getMinimo().getY())/np;
	}

	@Override
	public void movimenta() {
		w = (float) (0.9 - (iteracao / 1000.0) * 0.5);
		// w = 0.4f;
		if (generalista) {
			// TODO
			w1 = 0.0;
			w2 = 0.7;
			w3 = (float) (0.9 - (iteracao / 1000.0) * 0.9);
			w4 = 0.7;
			w5 = 0.7;
			c1 = 2.0;
			c2 = 1.4;
			// w5 = (float) (0.9 - (iteracao / 1000.0) * 0.8);
			// TODO
		} else {
			w4 = 0.7;
			w1 = w2 = w3 = w5 = 0;
			w = (float) (0.9 - (iteracao / 1000.0) * 0.5);
			c1 = 2.0;
			c2 = 1.4;
		}
		Presa presa;
		for (Particula particula : enxame) {
			presa = (Presa) particula;

			/*
			 * velocidade = w velocidade + w1 v1 + w2 v2 + w3 v3 + w4 v4
			 */

			particula.getVelocidade().setX(
					w * particula.getVelocidade().getX() + w1
							* presa.getV1().getX() + w2 * presa.getV2().getX()
							+ w3 * presa.getV3().getX() + w4
							* presa.getV4().getX() + w5 * presa.getV5().getX());
			particula.getVelocidade().setY(
					w * particula.getVelocidade().getY() + w1
							* presa.getV1().getY() + w2 * presa.getV2().getY()
							+ w3 * presa.getV3().getY() + w4
							* presa.getV4().getY() + w5 * presa.getV5().getY());

			velocidadeLimite(particula);
			
			/* posicao = posicao + velocidade */
			particula.getPosicao().setX(
					particula.getPosicao().getX()
							+ particula.getVelocidade().getX());
			particula.getPosicao().setY(
					particula.getPosicao().getY()
							+ particula.getVelocidade().getY());
			
			mantemNaTela(particula);
			
			iteracoesSemMelhoria++;
		}

	}

	@Override
	public void calcula() {
		Presa presa;
		pso();
		for (Particula particula : enxame) {
			presa = (Presa) particula;
			presa.setV1(regra1(particula));
			presa.setV2(regra2(particula));
			presa.setV3(regra3(particula));
			presa.setV4(regraPSO(particula));
			presa.setV5(evitarPredadores(particula));
		}
	}

	public void pso() {
		Posicao aux = new Posicao();
		double resultado = 0;
		for (Particula particula : enxame) {
			aux = particula.getPosicao();
			resultado = funcao.execute(aux.getX(), aux.getY());

			if (resultado < particula.getMerito()) {
				particula.getMelhor().setX(particula.getPosicao().getX());
				particula.getMelhor().setY(particula.getPosicao().getY());
				particula.setMerito(resultado);
				if (resultado < meritoGlobal) {
					melhorGlobal.setX(particula.getPosicao().getX());
					melhorGlobal.setY(particula.getPosicao().getY());
					meritoGlobal = resultado;
					iteracoesSemMelhoria = 0;
				}
			}
		}
	}

	private Posicao regraPSO(Particula particula) {
		Posicao posicao = new Posicao();
		posicao.setX((c1 * random.nextFloat() * (melhorGlobal.getX() - particula
				.getPosicao().getX()))
				+ (c2 * random.nextFloat() * (particula.getMelhor().getX() - particula
						.getPosicao().getX())));
		posicao.setY((c1 * random.nextFloat() * (melhorGlobal.getY() - particula
				.getPosicao().getY()))
				+ (c2 * random.nextFloat() * (particula.getMelhor().getY() - particula
						.getPosicao().getY())));
		return posicao;
	}

	private Posicao regra1(Particula particula) {
		Posicao centroMassa = new Posicao(0, 0);
		for (Particula p : enxame) {
			if (!particula.equals(p)) {
				centroMassa.setX(centroMassa.getX() + p.getPosicao().getX());
				centroMassa.setY(centroMassa.getY() + p.getPosicao().getY());
			}
		}
		centroMassa.setX(centroMassa.getX() / (enxame.size() - 1));
		centroMassa.setY(centroMassa.getY() / (enxame.size() - 1));
		centroMassa.setX(centroMassa.getX() - particula.getPosicao().getX());
		centroMassa.setY(centroMassa.getY() - particula.getPosicao().getY());
		return centroMassa;
	}

	private Posicao regra2(Particula particula) {
		Posicao posicao = new Posicao(0, 0);
		double x, y;
		for (Particula p : enxame) {
			double dist_euc = 0;
			if (!particula.equals(p)) {
				x = particula.getPosicao().getX() - p.getPosicao().getX();
				x *= x;
				y = particula.getPosicao().getY() - p.getPosicao().getY();
				y *= y;
				dist_euc = Math.sqrt(x + y);
				if (dist_euc < distancia) {
					posicao.setX(posicao.getX()
							- ((p.getPosicao().getX() - particula.getPosicao()
									.getX())));
					posicao.setY(posicao.getY()
							- ((p.getPosicao().getY() - particula.getPosicao()
									.getY())));
				}
			}
		}
		return posicao;

	}

	private Posicao regra3(Particula particula) {
		Posicao soma = new Posicao(0, 0);
		for (Particula p : enxame) {
			if (!particula.equals(p)) {
				soma.setX(soma.getX() + p.getVelocidade().getX());
				soma.setY(soma.getY() + p.getVelocidade().getY());
			}
		}
		soma.setX((soma.getX() / (enxame.size() - 1)));
		soma.setY((soma.getY() / (enxame.size() - 1)));
		return soma;
	}

	@Override
	public void inicializa() {
		this.meritoGlobal = Double.MAX_VALUE;
		for (int i = 0; i < np; i++) {
			enxame.add(new Presa(new Posicao(getFuncao().getMinimo().getX()
					+ Math.abs(random.nextDouble())
					* (getFuncao().getMaximo().getX() - getFuncao().getMinimo()
							.getX()), getFuncao().getMinimo().getY()
					+ Math.abs(random.nextDouble())
					* (getFuncao().getMaximo().getY() - getFuncao().getMinimo()
							.getY())),cor));
		}
	}

	private Posicao evitarPredadores(Particula particula) {
		Posicao posicao = new Posicao(0, 0);
		double x, y;
		for (Posicao p : predadores) {
			double dist_euc = 0;
			x = particula.getPosicao().getX() - p.getX();
			x *= x;
			y = particula.getPosicao().getY() - p.getY();
			y *= y;
			dist_euc = Math.sqrt(x + y);
			if (dist_euc < repulsao) {
				posicao.setX(posicao.getX()
						- ((p.getX() - particula.getPosicao().getX())));
				posicao.setY(posicao.getY()
						- ((p.getY() - particula.getPosicao().getY())));
			}
		}
		return posicao;
	}

	public List<Posicao> getPredadores() {
		return predadores;
	}

	public void setPredadores(List<Posicao> predadores) {
		// this.predadores = predadores;
		for (Posicao posicao : predadores) {
			this.predadores.add(posicao);
		}
	}

	public double getW1() {
		return w1;
	}

	public void setW1(double w1) {
		this.w1 = w1;
	}

	public double getW2() {
		return w2;
	}

	public void setW2(double w2) {
		this.w2 = w2;
	}

	public double getW3() {
		return w3;
	}

	public void setW3(double w3) {
		this.w3 = w3;
	}

	public double getW4() {
		return w4;
	}

	public void setW4(double w4) {
		this.w4 = w4;
	}

	public double getW5() {
		return w5;
	}

	public void setW5(double w5) {
		this.w5 = w5;
	}

	public double getC1() {
		return c1;
	}

	public void setC1(double c1) {
		this.c1 = c1;
	}

	public double getC2() {
		return c2;
	}

	public void setC2(double c2) {
		this.c2 = c2;
	}

	public int getIteracoesSemMelhoria() {
		return iteracoesSemMelhoria;
	}

	public void setIteracoesSemMelhoria(int iteracoesSemMelhoria) {
		this.iteracoesSemMelhoria = iteracoesSemMelhoria;
	}

	public boolean isGeneralista() {
		return generalista;
	}

	public void setGeneralista(boolean generalista) {
		this.generalista = generalista;
	}

}
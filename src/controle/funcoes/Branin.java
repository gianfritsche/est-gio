package controle.funcoes;

import modelo.Posicao;

public class Branin extends Funcao {

	public Branin() {
		minimo = new Posicao(-5, 0);
		maximo = new Posicao(10, 15);
		otimos.add(new Posicao(-Math.PI, 12.275));
		otimos.add(new Posicao(Math.PI, 2.275));
		otimos.add(new Posicao(9.42478, 2.475));
		valorOtimo = 0;
	}

	@Override
	public double execute(double x, double y) {
		return ((y - (5.1 / (4 * Math.PI * Math.PI)) * x * x + (5 / Math.PI)
				* x - 6)
				* (y - (5.1 / (4 * Math.PI * Math.PI)) * x * x + (5 / Math.PI)
						* x - 6) + (10 * (1 - 1 / 8 * Math.PI) * Math.cos(x) + 10));
	}

}

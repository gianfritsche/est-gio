package controle.funcoes;

public class FabricaFuncoes {

	public static final int RASTRINGIN = 0;
	public static final int SPHERE = 1;
	public static final int XY = 2;
	public static final int BRANIN = 3;
	public static final int ROSENBROCK = 4;
	public static final int GRIEWANK = 5;
	public static final int SHUBERT = 6;

	public Funcao getFuncao(int funcao) {
		switch (funcao) {
		case RASTRINGIN:
			return new Rastringin();
		case SPHERE:
			return new Sphere();
		case XY:
			return new XY();
		case BRANIN:
			return new Branin();
		case ROSENBROCK:
			return new Rosenbrock();
		case GRIEWANK:
			return new Griewank();
		case SHUBERT:
			return new Shubert();
		}
		return null;
	}

}

package controle.funcoes;

import java.util.ArrayList;
import java.util.List;

import modelo.Posicao;

public abstract class Funcao {

	protected Posicao minimo, maximo;
	protected List<Posicao> otimos;
	protected double valorOtimo;
	
	public Funcao() {
		minimo = new Posicao();
		maximo = new Posicao();
		otimos = new ArrayList<Posicao>();
		valorOtimo = 0;
	}
	
	public abstract double execute(double x, double y);

	public Posicao getMinimo() {
		return minimo;
	}

	public void setMinimo(Posicao minimo) {
		this.minimo = minimo;
	}

	public Posicao getMaximo() {
		return maximo;
	}

	public void setMaximo(Posicao maximo) {
		this.maximo = maximo;
	}

	public List<Posicao> getOtimos() {
		return otimos;
	}

	public void setOtimos(List<Posicao> otimos) {
		this.otimos = otimos;
	}

	public double getValorOtimo() {
		return valorOtimo;
	}

	public void setValorOtimo(double valorOtimo) {
		this.valorOtimo = valorOtimo;
	}

}

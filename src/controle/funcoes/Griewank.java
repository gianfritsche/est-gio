package controle.funcoes;

import modelo.Posicao;

public class Griewank extends Funcao {

	public Griewank() {
		minimo = new Posicao(-600, -600);
		maximo = new Posicao(600, 600);
		otimos.add(new Posicao(0, 0));
		valorOtimo = 0;
	}
	
	@Override
	public double execute(double x, double y) {
		double score = 1;
		double prod = 1.0;
		score += (x * x) / 4000.0;
		score += (y * y) / 4000.0;
		prod *= Math.cos(x / Math.sqrt(0 + 1.0));
		prod *= Math.cos(y / Math.sqrt(1 + 1.0));
		score -= prod;
		return -(1/(score + 1));
	}
}

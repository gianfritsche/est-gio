package controle.funcoes;

import modelo.Posicao;

public class Rastringin extends Funcao {

	public Rastringin() {
		minimo = new Posicao(-5.12, -5.12);
		maximo = new Posicao(5.12, 5.12);
		otimos.add(new Posicao(0, 0));
		valorOtimo = 0;
	}

	@Override
	public double execute(double x, double y) {
		return ((x * x) - 10 * Math.cos(2 * Math.PI * (x)) + 10 + (y * y) - 10
				* Math.cos(2 * Math.PI * (y)) + 10);
		// return - ((x * x) - 10 * Math.cos(2 * Math.PI * (x)) + 10 + (y * y) -
		// 10
		// * Math.cos(2 * Math.PI * (y)) + 10);
	}

}

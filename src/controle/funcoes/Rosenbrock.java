package controle.funcoes;

import modelo.Posicao;

public class Rosenbrock extends Funcao {

	public Rosenbrock() {
		minimo = new Posicao(-5, -5);
		maximo = new Posicao(10, 10);
		valorOtimo = 0;
	}

	@Override
	public double execute(double x, double y) {
		return ((1 - x) * (1 - x)) + 100 * (y - (x * x)) * (y - (x * x));
	}
}

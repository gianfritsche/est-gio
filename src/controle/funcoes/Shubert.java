package controle.funcoes;

import modelo.Posicao;

public class Shubert extends Funcao {

	public Shubert() {
		minimo = new Posicao(-10, -10);
		maximo = new Posicao(10, 10);
		valorOtimo = -186.73;
	}

	@Override
	public double execute(double x1, double x2) {
		double soma1=0, soma2 = 0;
		for (int i = 0; i <= 5; i++) {
			soma1 += i * Math.cos((i + 1) * x1 + i);
			soma2 += i * Math.cos((i + 1) * x2 + i);
		}
		return soma1*soma2;
	}

}

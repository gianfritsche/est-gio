package controle.funcoes;

import modelo.Posicao;

public class Sphere extends Funcao {

	public Sphere() {
		minimo = new Posicao(-5.12, -5.12);
		maximo = new Posicao(5.12, 5.12);
		otimos.add(new Posicao(0, 0));
		valorOtimo = 0;
	}

	@Override
	public double execute(double x, double y) {
		return x * x + y * y;
	}

}

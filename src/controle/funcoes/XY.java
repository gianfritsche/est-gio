package controle.funcoes;

import modelo.Posicao;

public class XY extends Funcao {

	public XY() {
		minimo = new Posicao(-5.12, -5.12);
		maximo = new Posicao(5.12, 5.12);
		otimos.add(new Posicao(100, 100));
		valorOtimo = -10.24;
	}

	@Override
	public double execute(double x, double y) {
		return x + y;
	}

}

package controle.persistencia;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import modelo.Particula;
import modelo.Pirilampo;

public class GuardaDadosGlowworm implements IGuarda {

	private FileWriter fileLuciferin;
	private FileWriter fileMerito;
	private FileWriter filePosicao;

	private PrintWriter printLuciferin;
	private PrintWriter printMerito;
	private PrintWriter printPosicao;
	
	private String file = "/home/gian/Documentos/intercambio/UTAD/projeto/relatorio2/Resultados/Teste/";
	public GuardaDadosGlowworm() {
		inicializa();
	}

	@Override
	public void inicializa() {
		try {
			fileLuciferin = new FileWriter(file + "luciferin.dat");
			fileMerito = new FileWriter(file + "merito.dat");
			filePosicao = new FileWriter(file + "posicao.dat");
			printLuciferin = new PrintWriter(fileLuciferin);
			printMerito = new PrintWriter(fileMerito);
			printPosicao = new PrintWriter(filePosicao);
		} catch (IOException e) {
//			e.printStackTrace();
			System.out.println("N�o poss�vel encontrar os arquivos de registro, as informa��es da simula��o n�o ser�o armazenadas!");
		}
	}

	@Override
	public void guarda(List<Particula> enxame) {
		for (Particula particula : enxame) {
			Pirilampo pirilampo = (Pirilampo) particula;
			printLuciferin.print(pirilampo.getLuciferin() + " ");
			printMerito.print(pirilampo.getMerito() + " ");
			printPosicao.print(pirilampo.getPosicao().getX() + " "
					+ pirilampo.getPosicao().getY() + " ");
		}
		printLuciferin.println();
		printMerito.println();
		printPosicao.println();
	}

	@Override
	public void encerra() {
		printLuciferin.close();
		printMerito.close();
		printPosicao.close();
		try {
			fileLuciferin.close();
			fileMerito.close();
			filePosicao.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}

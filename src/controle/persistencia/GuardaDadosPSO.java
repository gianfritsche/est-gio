package controle.persistencia;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import modelo.Particula;

public class GuardaDadosPSO implements IGuarda {

	private FileWriter fileMerito;
	private FileWriter filePosicao;

	private PrintWriter printMerito;
	private PrintWriter printPosicao;
	
	private String file = "/home/gian/Documentos/intercambio/UTAD/projeto/relatorio2/Resultados/PSO/";
	public GuardaDadosPSO() {
		
		inicializa();
	}

	@Override
	public void inicializa() {
		try {
			fileMerito = new FileWriter(file + "merito.dat");
			filePosicao = new FileWriter(file + "posicao.dat");
			printMerito = new PrintWriter(fileMerito);
			printPosicao = new PrintWriter(filePosicao);
		} catch (IOException e) {
//			e.printStackTrace();
			System.out.println("N�o poss�vel encontrar os arquivos de registro, as informa��es da simula��o n�o ser�o armazenadas!");
		}
	}

	@Override
	public void encerra() {
		printMerito.close();
		printPosicao.close();
		try {
			fileMerito.close();
			filePosicao.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void guarda(List<Particula> enxame) {
		for (Particula particula : enxame) {
			printMerito.print(particula.getMerito() + " ");
			printPosicao.print(particula.getPosicao().getX() + " "
					+ particula.getPosicao().getY() + " ");
		}
		printMerito.println();
		printPosicao.println();	
	}

}

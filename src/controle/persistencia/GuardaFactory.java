package controle.persistencia;


public class GuardaFactory {
	public static final int GLOWWORM = 0;
	public static final int PSO = 1;
	
	public static IGuarda getGuarda(int opcao){
		switch(opcao){
		case GLOWWORM:
			return new GuardaDadosGlowworm();
		case PSO:
			return new GuardaDadosPSO();
		default:
			return null;
		}
		
	}
}

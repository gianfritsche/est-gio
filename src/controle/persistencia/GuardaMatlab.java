package controle.persistencia;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import modelo.Particula;
import modelo.Posicao;
import controle.algoritmos.Algoritmo;

public class GuardaMatlab implements IGuarda {

	private Algoritmo algoritmo;
	private FileWriter fileWriter;
	private PrintWriter printWriter;
	private String file = "/home/gian/Documentos/intercambio/UTAD/projeto/relatorio2/octave/arquivo.m";

	public GuardaMatlab(Algoritmo algoritmo) throws IOException {
		this.algoritmo = algoritmo;
		fileWriter = new FileWriter(file);
		printWriter = new PrintWriter(fileWriter);
		inicializa();
	}

	@Override
	public void inicializa() {
		printWriter.println("%Arquivo gerado automaticamente");
		printWriter.println("clc");
		printWriter.println("close all");
		printWriter.println("clear all");
		printWriter.println("xparticulas=zeros(" + algoritmo.getEnxame().size()
				+ ", " + algoritmo.getMaxIteracoes() + ");");
		printWriter.println("yparticulas=zeros(" + algoritmo.getEnxame().size()
				+ ", " + algoritmo.getMaxIteracoes() + ");");
		printWriter.println("valor=zeros(" + algoritmo.getEnxame().size()
				+ ", " + algoritmo.getMaxIteracoes() + ");");
		printWriter.println("xpontos=zeros("
				+ algoritmo.getFuncao().getOtimos().size() + ", "
				+ algoritmo.getMaxIteracoes() + ");");
		printWriter.println("ypontos=zeros("
				+ algoritmo.getFuncao().getOtimos().size() + ", "
				+ algoritmo.getMaxIteracoes() + ");");
		printWriter.println("xmedia=zeros(1," + algoritmo.getMaxIteracoes()
				+ ");");
		printWriter.println("ymedia=zeros(1," + algoritmo.getMaxIteracoes()
				+ ");");
		printWriter.println("xmelhor=zeros(1," + algoritmo.getMaxIteracoes()
				+ ");");
		printWriter.println("ymelhor=zeros(1," + algoritmo.getMaxIteracoes()
				+ ");");
		printWriter.println("melhorValor=zeros(1,"
				+ algoritmo.getMaxIteracoes() + ");");

		int i = 1;
		for (Posicao posicao : algoritmo.getFuncao().getOtimos()) {
			printWriter.println("xpontos(" + i + ",:)=" + posicao.getX() + ";");
			printWriter.println("ypontos(" + i + ",:)=" + posicao.getY() + ";");
			i++;
		}
	}

	@Override
	public void guarda(List<Particula> enxame) {
		int i = 1;
		int iteracao = algoritmo.getIteracao() + 1;
		double mediax = 0.0, mediay = 0.0, melhor = Double.MAX_VALUE;
		for (Particula particula : algoritmo.getEnxame()) {
			printWriter.println("xparticulas(" + i + "," + iteracao + ")="
					+ particula.getPosicao().getX() + ";");
			printWriter.println("yparticulas(" + i + "," + iteracao + ")="
					+ particula.getPosicao().getY() + ";");
			printWriter.println("valor(" + i + "," + iteracao + ")="
					+ particula.getMerito() + ";");
			melhor = (particula.getMerito() < melhor) ? (particula.getMerito())
					: (melhor);
			mediax += particula.getPosicao().getX();
			mediay += particula.getPosicao().getY();
			i++;
		}
		printWriter.println("xmelhor(1," + iteracao + ")="
				+ algoritmo.getMelhorGlobal().getX() + ";");
		printWriter.println("ymelhor(1," + iteracao + ")="
				+ algoritmo.getMelhorGlobal().getX() + ";");
		printWriter.println("xmedia(1," + iteracao + ")=" + mediax
				/ ((double) algoritmo.getEnxame().size()) + ";");
		printWriter.println("ymedia(1," + iteracao + ")=" + mediay
				/ ((double) algoritmo.getEnxame().size()) + ";");
		printWriter.println("melhorValor(1," + iteracao + ")=" + melhor + ";");

	}

	@Override
	public void encerra() {

		printWriter.println("figure(1);");
		printWriter
				.println("for t=1:"
						+ algoritmo.getEnxame().size()
						+ " \n \t plot(xparticulas(t,:), 'k--'); \n \t hold on; \n \t 	end");
		printWriter.println("plot(xmedia(1,:), 'r-');");
		printWriter.println("plot(xmelhor(1,:), 'b-');");
		printWriter.println("figure(2);");
		printWriter
				.println("for t=1:"
						+ algoritmo.getEnxame().size()
						+ " \n \t plot(yparticulas(t,:), 'k-'); \n \t hold on; \n \t 	end");
		printWriter.println("plot(ymedia(1,:), 'r-');");
		printWriter.println("plot(ymelhor(1,:), 'b-');");

		printWriter.println("figure(3);");
		printWriter
				.println("for t=1:"
						+ algoritmo.getEnxame().size()
						+ " \n \t plot(valor(t,:), 'k-'); \n \t hold on; \n \t 	end");
		printWriter.println("plot(melhorValor(1,:), 'b-');");

		printWriter.close();
		try {
			fileWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}

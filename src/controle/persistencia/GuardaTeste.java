package controle.persistencia;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import modelo.Particula;
import modelo.Posicao;
import controle.algoritmos.Algoritmo;

public class GuardaTeste implements IGuarda{

	private List<FileWriter> fw;
	private List<PrintWriter> pw;
	private String caminho;
	private Algoritmo algoritmo;

	public GuardaTeste(Algoritmo algoritmo) {
		this.algoritmo = algoritmo;
		pw = new ArrayList<PrintWriter>();
		fw = new ArrayList<FileWriter>();
		caminho = new String(
				"/home/gian/Documentos/intercambio/UTAD/projeto/relatorio2/graficos/"
						+ algoritmo.getClass().getSimpleName() + "/"
						+ algoritmo.getFuncao().getClass().getSimpleName()
						+ "/");
		inicializa();

	}

	public void inicializa() {
		try {
			fw.add(new FileWriter(caminho + "Iteracao_ValorOtimo"));
			fw.add(new FileWriter(caminho + "Iteracao_MelhorValor"));
			for (int i = 0; i < algoritmo.getFuncao().getOtimos().size(); i++) {
				fw.add(new FileWriter(caminho + "Iteracao_XOtimo" + i));
				fw.add(new FileWriter(caminho + "Iteracao_YOtimo" + i));
			}
			fw.add(new FileWriter(caminho + "Iteracao_MelhorX"));
			fw.add(new FileWriter(caminho + "Iteracao_MelhorY"));
			for (int i = 0; i < algoritmo.getFuncao().getOtimos().size(); i++) {
				fw.add(new FileWriter(caminho + "Iteracao_XOtimo_YOtimo" + i));
			}
			fw.add(new FileWriter(caminho + "Iteracao_MelhorX_MelhorY"));
			fw.add(new FileWriter(caminho + "Iteracao_CentroMassa"));
			for (int i = 0; i < fw.size(); i++) {
				pw.add(new PrintWriter(fw.get(i)));
			}
		} catch (IOException e) {
//			e.printStackTrace();
			System.out.println("N�o poss�vel encontrar os arquivos de registro, as informa��es da simula��o n�o ser�o armazenadas!");
		}
	}

	public void guarda(List<Particula> enxame) {
		int i = 0;
		pw.get(i++).println(
				algoritmo.getIteracao() + " "
						+ algoritmo.getFuncao().getValorOtimo() + " i");
		pw.get(i++).println(
				algoritmo.getIteracao() + " " + algoritmo.getMeritoGlobal()
						+ " i");
		for (Posicao posicao : algoritmo.getFuncao().getOtimos()) {
			pw.get(i++).println(
					algoritmo.getIteracao() + " " + posicao.getX() + " i");
			pw.get(i++).println(
					algoritmo.getIteracao() + " " + posicao.getY() + " i");
		}
		pw.get(i++).println(
				algoritmo.getIteracao() + " "
						+ algoritmo.getMelhorGlobal().getX() + " i");
		pw.get(i++).println(
				algoritmo.getIteracao() + " "
						+ algoritmo.getMelhorGlobal().getY() + " i");
		for (Posicao posicao : algoritmo.getFuncao().getOtimos()) {
			pw.get(i++).println(
					algoritmo.getIteracao() + " " + posicao.getX() + " "
							+ posicao.getY() + " i");
		}
		pw.get(i++).println(
				algoritmo.getIteracao() + " "
						+ algoritmo.getMelhorGlobal().getX() + " "
						+ algoritmo.getMelhorGlobal().getY() + " i");
		pw.get(i++).println(
				algoritmo.getIteracao() + " "
						+ algoritmo.getCentroMassa().getX() + " "
						+ algoritmo.getCentroMassa().getY() + " i");

	}

	public void encerra() {

		for (int i = 0; i < pw.size(); i++) {
			pw.get(i).close();
			try {
				fw.get(i).close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

}

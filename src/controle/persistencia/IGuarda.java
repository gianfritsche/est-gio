package controle.persistencia;

import java.util.List;

import modelo.Particula;

public interface IGuarda {

	public void inicializa();

	public void guarda(List<Particula> enxame);
	
	public void encerra();

}

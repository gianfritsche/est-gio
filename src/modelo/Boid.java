package modelo;

public class Boid extends Particula {

	private Posicao v1;
	private Posicao v2;
	private Posicao v3;
	private Posicao v4;	
	
	public Boid(Posicao posicao) {
		super(posicao);
	}

	public Posicao getV1() {
		return v1;
	}

	public void setV1(Posicao v1) {
		this.v1 = v1;
	}

	public Posicao getV2() {
		return v2;
	}

	public void setV2(Posicao v2) {
		this.v2 = v2;
	}

	public Posicao getV3() {
		return v3;
	}

	public void setV3(Posicao v3) {
		this.v3 = v3;
	}

	public Posicao getV4() {
		return v4;
	}

	public void setV4(Posicao v4) {
		this.v4 = v4;
	}

}

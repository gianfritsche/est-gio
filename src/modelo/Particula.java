package modelo;

import java.awt.Color;

public class Particula {

	// Atributos
	private Posicao posicao, velocidade, melhor;
	private double merito;
	private Color cor;

	// Construtor
	public Particula(Posicao posicao) {
		this.posicao = posicao;
		this.velocidade = new Posicao();
		this.melhor = new Posicao();
		this.merito = Double.MAX_VALUE;
		this.cor = Color.black;
	}

	// Metodos de Acesso
	public Posicao getPosicao() {
		return posicao;
	}

	public void setPosicao(Posicao posicao) {
		this.posicao = posicao;
	}

	public Posicao getVelocidade() {
		return velocidade;
	}

	public void setVelocidade(Posicao velocidade) {
		this.velocidade = velocidade;
	}

	public Posicao getMelhor() {
		return melhor;
	}

	public void setMelhor(Posicao melhor) {
		this.melhor = melhor;
	}

	public double getMerito() {
		return merito;
	}

	public void setMerito(double merito) {
		this.merito = merito;
	}

	public void setCor(Color cor){
		this.cor = cor;
	}
	
	public Color getCor(){
		return this.cor;
	}
	
}

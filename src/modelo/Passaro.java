package modelo;

import java.util.List;

public class Passaro extends Pirilampo {

	private Posicao velocidade;
	private Posicao v1;
	private Posicao v2;
	private Posicao v3;
	private Posicao v4;
	
	/** delta = 1.0 **/
	private static double dist = 1.0;

	/** delta = 0.5 **/
//	private static double dist = 0.5;

/** alpha = 0.2**/
	private static double porcv1 = 0.2;
	private static double porcv2 = 0.2;
	private static double porcv3 = 0.2;
	private static double porcv4 = 0.2;

/** alpha = 0.1 **/
//	private static double porcv1 = 0.1;
//	private static double porcv2 = 0.1;
//	private static double porcv3 = 0.1;
//	private static double porcv4 = 0.1;

	public static double w=0.9;
	private Pirilampo objetivo;

	public Passaro(Posicao posicao, Pirilampo objetivo) {
		super(posicao);
		this.objetivo = objetivo;
		this.velocidade = new Posicao();
	}

	public static double getPorcv1() {
		return porcv1;
	}

	public static void setPorcv1(double porcv1) {
		Passaro.porcv1 = porcv1;
	}

	public static double getPorcv2() {
		return porcv2;
	}

	public static void setPorcv2(double porcv2) {
		Passaro.porcv2 = porcv2;
	}

	public static double getPorcv3() {
		return porcv3;
	}

	public static void setPorcv3(double porcv3) {
		Passaro.porcv3 = porcv3;
	}

	public Posicao getVelocidade() {
		return velocidade;
	}

	public void setVelocidade(Posicao velocidade) {
		this.velocidade = velocidade;
	}

	public void movimenta() {
		velocidade.setX(w*velocidade.getX() + v1.getX() + v2.getX() + v3.getX()+ v4.getX());
		velocidade.setY(w*velocidade.getY() + v1.getY() + v2.getY() + v3.getY()+ v4.getY());

		this.getPosicao().setX(this.getPosicao().getX() + velocidade.getX());
		this.getPosicao().setY(this.getPosicao().getY() + velocidade.getY());

	}
	public void movimenta(int iteracao) {
		w = (float) (0.9 - (iteracao / 200.0) * 0.5);
		velocidade.setX(w*velocidade.getX() + v1.getX() + v2.getX() + v3.getX()+ v4.getX());
		velocidade.setY(w*velocidade.getY() + v1.getY() + v2.getY() + v3.getY()+ v4.getY());

		this.getPosicao().setX(this.getPosicao().getX() + velocidade.getX());
		this.getPosicao().setY(this.getPosicao().getY() + velocidade.getY());

	}

	public void calcula(List<Passaro> passaros) {
		v1 = regra1(passaros);
		v2 = regra2(passaros);
		v3 = regra3(passaros);
		v4 = aproximarObjetivo();
	}

	private Posicao regra1(List<Passaro> passaros) {

		if (passaros.size() < 2)
			return new Posicao(0, 0);
		else {
			Posicao centroMassa = new Posicao(0, 0);
			for (Passaro passaro : passaros) {
				if (!this.equals(passaro)) {
					centroMassa.setX(centroMassa.getX()
							+ passaro.getPosicao().getX());
					centroMassa.setY(centroMassa.getY()
							+ passaro.getPosicao().getY());
				}
			}

			centroMassa.setX(centroMassa.getX() / (passaros.size() - 1));
			centroMassa.setY(centroMassa.getY() / (passaros.size() - 1));
			centroMassa.setX(centroMassa.getX() - this.getPosicao().getX());
			centroMassa.setY(centroMassa.getY() - this.getPosicao().getY());
			centroMassa.setX(centroMassa.getX() * porcv1);
			centroMassa.setY(centroMassa.getY() * porcv1);

			return centroMassa;
		}
	}

	private Posicao regra2(List<Passaro> passaros) {
		Posicao p = new Posicao(0, 0);
		double x;
		double y;
		for (Passaro passaro : passaros) {
			double dist_euc = 0;
			if (!this.equals(passaro)) {
				x = this.getPosicao().getX() - passaro.getPosicao().getX();
				x *= x;
				y = this.getPosicao().getY() - passaro.getPosicao().getY();
				y *= y;
				dist_euc = (double) Math.sqrt(x + y);
				if (dist_euc < getDist()) {
					p.setX(p.getX()
							- ((passaro.getPosicao().getX() - this.getPosicao()
									.getX()) * porcv2));
					p.setY(p.getY()
							- ((passaro.getPosicao().getY() - this.getPosicao()
									.getY()) * porcv2));
				}
			}
		}
		return p;
	}

	private Posicao regra3(List<Passaro> passaros) {
		Posicao soma = new Posicao(0, 0);
		for (Passaro passaro : passaros) {
			if (!this.equals(passaro)) {
				soma.setX(soma.getX() + passaro.getVelocidade().getX());
				soma.setY(soma.getY() + passaro.getVelocidade().getY());
			}
		}
		soma.setX((soma.getX() / (passaros.size() - 1)));
		soma.setY((soma.getY() / (passaros.size() - 1)));

		soma.setX((soma.getX() - this.getVelocidade().getX()) * porcv3);
		soma.setY((soma.getY() - this.getVelocidade().getY()) * porcv3);
		return soma;
	}

	public static double getDist() {
		return dist;
	}

	public static void setDist(double dist) {
		Passaro.dist = dist;
	}

	public void setW(double value){
		w=value;
	}
	
	Posicao aproximarObjetivo() {
		Posicao r = new Posicao(0, 0);
		r.setX((objetivo.getPosicao().getX() - this.getPosicao().getX()) * porcv4);
		r.setY((objetivo.getPosicao().getY() - this.getPosicao().getY()) * porcv4);
		return r;
	}
}

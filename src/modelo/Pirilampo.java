package modelo;

import java.util.ArrayList;
import java.util.List;

public class Pirilampo extends Particula {

	private double p = 0.4, y = 0.6, B = 0.08, nt = 5.0, s = 0.03, rs = 3.0;
	private double luciferin, r;
	private List<Pirilampo> vizinhos;
	private Pirilampo vizinho;

	public Pirilampo(Posicao posicao) {
		super(posicao);
		this.luciferin = 5.0;
		this.r = 3.0;
		vizinhos = new ArrayList<Pirilampo>();	
	}

	public void atualizaLuciferin() {
		luciferin = (1.0 - p) * luciferin + y * (-getMerito());
	}

	public void determinaVizinhos(List<Particula> enxame) {
		double soma = 0;
		vizinhos.clear();
		for (Particula particula : enxame) {
			Pirilampo pirilampo = (Pirilampo) particula;
			if (calculaDistancia(this.getPosicao(),
							pirilampo.getPosicao()) < r
					&& pirilampo.luciferin > this.luciferin) {
				vizinhos.add(pirilampo);
				soma += pirilampo.luciferin - this.luciferin;
			}
		}

		double p = Double.MIN_VALUE;
		vizinho=this;
		for (Pirilampo pirilampo : vizinhos) {
			if ((pirilampo.luciferin - this.luciferin) / soma > p) {
				vizinho = pirilampo;
				p = (pirilampo.luciferin - this.luciferin) / soma;
			}
		}
	}
	
	public double calculaDistancia(Posicao p1, Posicao p2) {
		return Math.sqrt((p1.getX() - p2.getX()) * (p1.getX() - p2.getX())
				+ (p1.getY() - p2.getY()) * (p1.getY() - p2.getY()));
	}

	public void atualizaPosicao() {
		getPosicao().setX(
				getPosicao().getX()
						+ s
						* Math.signum(vizinho.getPosicao().getX()
								- getPosicao().getX()));
		getPosicao().setY(
				getPosicao().getY()
						+ s
						* Math.signum(vizinho.getPosicao().getY()
								- getPosicao().getY()));
	}

	public void atualizaRaio() {
		r = Math.min(rs, Math.max(0.0, r + B * ((double)(((double)nt) - ((double)vizinhos.size())))));
	}

	public void setS(double s){
		this.s = s;
	}
	
	public double getS(){
		return s;
	}

	public double getP() {
		return p;
	}

	public void setP(double p) {
		this.p = p;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	public double getB() {
		return B;
	}

	public void setB(double b) {
		B = b;
	}

	public double getNt() {
		return nt;
	}

	public void setNt(double nt) {
		this.nt = nt;
	}

	public double getRs() {
		return rs;
	}

	public void setRs(double rs) {
		this.rs = rs;
	}

	public double getLuciferin() {
		return luciferin;
	}

	public void setLuciferin(double luciferin) {
		this.luciferin = luciferin;
	}

	public double getR() {
		return r;
	}

	public void setR(double r) {
		this.r = r;
	}

	public List<Pirilampo> getVizinhos() {
		return vizinhos;
	}

	public void setVizinhos(List<Pirilampo> vizinhos) {
		this.vizinhos = vizinhos;
	}

	public Pirilampo getVizinho() {
		return vizinho;
	}

	public void setVizinho(Pirilampo vizinho) {
		this.vizinho = vizinho;
	}
	
}

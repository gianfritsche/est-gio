package modelo;

public class Posicao {

	private double x;
	private double y;

	public Posicao() {
		this.x = 0;
		this.y = 0;
	}

	public Posicao(double x, double y) {
		this.x = x;
		this.y = y;
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

}

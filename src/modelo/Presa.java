package modelo;

import java.awt.Color;

public class Presa extends Boid {

	private Posicao v5;

	public Presa(Posicao posicao, Color cor) {
		super(posicao);
		setCor(cor);
	}

	public Posicao getV5() {
		return v5;
	}

	public void setV5(Posicao v5) {
		this.v5 = v5;
	}

}

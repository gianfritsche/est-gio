package visao;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ConcurrentModificationException;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import controle.algoritmos.Algoritmo;
import controle.algoritmos.Boids;
import controle.algoritmos.Glowworm;
import controle.algoritmos.GlowwormBoids;
import controle.algoritmos.GlowwormBoidsPSO;
import controle.algoritmos.PSO;
import controle.funcoes.FabricaFuncoes;
import controle.persistencia.GuardaFactory;

public class Janela extends JFrame implements ActionListener {

	private static final long serialVersionUID = 1L;
	private Painel painel;
	private JPanel painelConfiguracoes;
	private JLabel iteracao;
	private JLabel lbFuncao;
	private JLabel lbMetodo;
	private JButton iniciar;
	private JComboBox<String> cbAlgoritmo;
	private JComboBox<String> funcao;
	private Algoritmo algoritmo;
	private FabricaFuncoes fabrica;

	public Janela() {
		init();
		fabrica = new FabricaFuncoes();
		algoritmo = new PSO(fabrica.getFuncao(funcao.getSelectedIndex()), this, GuardaFactory.getGuarda(GuardaFactory.PSO));
	}

	private void init() {
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setExtendedState(JFrame.MAXIMIZED_BOTH);
		this.setLayout(new BorderLayout());
		painel = new Painel();
		this.add(painel, BorderLayout.CENTER);
		painelConfiguracoes = new JPanel();
		painelConfiguracoes.setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.insets = new Insets(10, 0, 10, 10);
		painelConfiguracoes.setBackground(Color.black);
		painelConfiguracoes.setSize(150, 700);
		this.add(painelConfiguracoes, BorderLayout.WEST);
		gbc.gridy = 0;
		gbc.gridx = 0;
		lbFuncao = new JLabel("Fun��o:");
		lbFuncao.setForeground(Color.WHITE);
		lbMetodo = new JLabel("M�todo:");
		lbMetodo.setForeground(Color.WHITE);
		painelConfiguracoes.add(lbMetodo, gbc);
		String[] algoritmos = new String[5];
		algoritmos[0] = "  PSO";
		algoritmos[1] = "GlowWorm";
		algoritmos[2] = "GlowWormBoids";
		algoritmos[3] = "GlowWormBoidsPSO";
		algoritmos[4] = "  Boids";
		cbAlgoritmo = new JComboBox<String>(algoritmos);
		gbc.gridx = 1;
		painelConfiguracoes.add(cbAlgoritmo, gbc);
		String[] funcoes = new String[7];
		funcoes[FabricaFuncoes.RASTRINGIN] = "rastringin";
		funcoes[FabricaFuncoes.SPHERE] = "  sphere";
		funcoes[FabricaFuncoes.XY] = "   x+y";
		funcoes[FabricaFuncoes.BRANIN] = " branin";
		funcoes[FabricaFuncoes.ROSENBROCK] = "rosenbrock";
		funcoes[FabricaFuncoes.GRIEWANK] = "griewank";
		funcoes[FabricaFuncoes.SHUBERT] = "shubert";
		funcao = new JComboBox<String>(funcoes);
		gbc.gridy++;
		gbc.gridx = 0;
		painelConfiguracoes.add(lbFuncao, gbc);
		gbc.gridx = 1;
		painelConfiguracoes.add(funcao, gbc);
		iniciar = new JButton("Iniciar");
		iniciar.addActionListener(this);
		gbc.weightx = 2;
		gbc.gridwidth = 2;
		gbc.gridy++;
		gbc.gridx = 0;
		painelConfiguracoes.add(iniciar, gbc);
		iteracao = new JLabel("Itera��o: 0");
		gbc.gridy++;
		iteracao.setForeground(Color.WHITE);
		painelConfiguracoes.add(iteracao, gbc);
		
	}

	public void desenha() {
		try {
			painel.draw(algoritmo);
			painel.paint();
			iteracao.setText("Itera��o: " + (algoritmo.getIteracao() + 1));
		} catch (ConcurrentModificationException e) {

		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		algoritmo.stop();
		if (cbAlgoritmo.getSelectedIndex() == 0) {
			algoritmo = new PSO(fabrica.getFuncao(funcao.getSelectedIndex()),
					this, GuardaFactory.getGuarda(GuardaFactory.PSO));
		} else if (cbAlgoritmo.getSelectedIndex() == 1) {
			algoritmo = new Glowworm(fabrica.getFuncao(funcao
					.getSelectedIndex()), this, GuardaFactory.getGuarda(GuardaFactory.GLOWWORM));

		} else if (cbAlgoritmo.getSelectedIndex() == 2) {
			algoritmo = new GlowwormBoids(fabrica.getFuncao(funcao
					.getSelectedIndex()), this, GuardaFactory.getGuarda(GuardaFactory.GLOWWORM));

		}else if (cbAlgoritmo.getSelectedIndex() == 3) {
			algoritmo = new GlowwormBoidsPSO(fabrica.getFuncao(funcao
					.getSelectedIndex()), this, GuardaFactory.getGuarda(GuardaFactory.GLOWWORM));

		}else if (cbAlgoritmo.getSelectedIndex() == 4) {
			algoritmo = new Boids(fabrica.getFuncao(funcao
					.getSelectedIndex()), this, GuardaFactory.getGuarda(GuardaFactory.GLOWWORM));

		}
		algoritmo.execute();
	}

	public Painel getPainel() {
		return painel;
	}

}

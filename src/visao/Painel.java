package visao;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import javax.swing.JPanel;
import modelo.Particula;
import modelo.Posicao;
import controle.algoritmos.Algoritmo;

public class Painel extends JPanel {

	private static final long serialVersionUID = 1L;
	private BufferedImage buffer;
	private Graphics2D bufferG;

	private Algoritmo algoritmo;

	public void draw(Algoritmo algoritmo) {
		this.algoritmo = algoritmo;

		if (buffer == null) {
			buffer = (BufferedImage) this.createImage(1100, 700);
			if (buffer != null) {
				bufferG = (Graphics2D) buffer.getGraphics();
			}
		}

		bufferG.setPaint(this.getBackground());
		bufferG.setColor(Color.white);
		bufferG.fillRect(0, 0, 2000, 1720);
		bufferG.setColor(Color.black);
		bufferG.drawRect((int) algoritmo.getMinimos().getX(), (int) algoritmo
				.getMinimos().getY(),
				(int) (algoritmo.getMaximos().getX() - algoritmo.getMinimos()
						.getX()),
				(int) (algoritmo.getMaximos().getY() - algoritmo.getMinimos()
						.getY()));
		Posicao posicao;
		for (Particula particula : algoritmo.getEnxame()) {
			posicao = convertePosicao(particula.getPosicao());
			bufferG.drawOval((int) posicao.getX() - 2,
					(int) posicao.getY() - 2, 4, 4);
		}
		for (Particula particula : algoritmo.getEnxame()) {
			bufferG.setColor(particula.getCor());
			posicao = convertePosicao(particula.getPosicao());
			bufferG.fillOval((int) posicao.getX() - 2,
					(int) posicao.getY() - 2, 4, 4);
		}
		
		bufferG.setColor(Color.red);
		for (Posicao p : algoritmo.getFuncao().getOtimos()) {
			posicao = convertePosicao(p);
			bufferG.drawOval((int) posicao.getX() - 5, (int) posicao.getY() - 5, 10, 10);
		}
	
	}

	public void paint() {
		Graphics2D g = (Graphics2D) this.getGraphics();

		if (g != null) {
			g.drawImage(buffer, 0, 0, null);
		}
	}

	public Posicao convertePosicao(Posicao posicao) {
		Posicao nova = new Posicao();
		nova.setX(((posicao.getX() - algoritmo.getFuncao().getMinimo().getX()) / (algoritmo
				.getFuncao().getMaximo().getX() - algoritmo.getFuncao()
				.getMinimo().getX()))
				* (algoritmo.getMaximos().getX() - algoritmo.getMinimos()
						.getX()) + algoritmo.getMinimos().getX());
		nova.setY(((posicao.getY() - algoritmo.getFuncao().getMinimo().getY()) / (algoritmo
				.getFuncao().getMaximo().getY() - algoritmo.getFuncao()
				.getMinimo().getY()))
				* (algoritmo.getMaximos().getY() - algoritmo.getMinimos()
						.getY()) + algoritmo.getMinimos().getY());
		return nova;
	}

}
